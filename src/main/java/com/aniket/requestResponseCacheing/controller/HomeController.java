package com.aniket.requestResponseCacheing.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.aniket.requestResponseCacheing.model.Student;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class HomeController {
	 
	@GetMapping("/Student")
	public List<Student> getStudentList()
	{
		log.info("----calling ----");
		List<Student> list = new ArrayList<>();
		Student std;
		for (int i = 0; i < 10; i++) {
			std = new Student();
			std.setId("1111"+i);
			std.setName("Sample"+i);
			std.setSubject("Sub"+i);
			list.add(std);
		}
		return list;
		
	}
	
	@GetMapping("/Student/{id}")
	public Student getStudentById(@PathVariable String id)
	{
		log.info("----calling ----{}",id);
		Student std;
		String i = id;
		std = new Student();
		std.setId("1111"+i);
		std.setName("Sample"+i);
		std.setSubject("Sub"+i);
	    return std;
		
	}
	@PostMapping("/Student")
	public String addStudent(@RequestBody Student std)
	{
		log.info("----calling ----{}",std);
	    return "Success";
		
	}
	
	@PutMapping("/Student/{id}")
	public Student updateStudentById(@PathVariable String id,@RequestBody Student std)
	{
		log.info("----calling ----{} -- {}",id,std);
		String i = id;
		std = new Student();
		std.setId("1111"+i);
		std.setName("Sample"+i);
		std.setSubject("Sub"+i);
	    return std;
		
	}
	
	@DeleteMapping("/Student/{id}")
	public Student deleteStudentById(@PathVariable String id)
	{
		
		log.info("----calling ----{}",id);
		Student std;
		String i = id;
		std = new Student();
		std.setId("1111"+i);
		std.setName("Sample"+i);
		std.setSubject("Sub"+i);
	    return std;
		
	}

}
