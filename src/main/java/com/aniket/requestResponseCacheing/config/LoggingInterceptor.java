package com.aniket.requestResponseCacheing.config;

import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.StreamUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LoggingInterceptor extends HandlerInterceptorAdapter {
	
	@Override
	public boolean preHandle(
	  HttpServletRequest request,
	  HttpServletResponse response, 
	  Object handler) throws Exception {
		 log.info("[preHandle][" + request + "]" + "[" + request.getMethod()
	      + "]" + request.getRequestURI());
		 InputStream inputStream = request.getInputStream();
	        byte[] body = StreamUtils.copyToByteArray(inputStream);
	        log.info("In PrintRequestContentFilter. Request body is: " + new String(body));
	    return true;
	}

}
