package com.aniket.requestResponseCacheing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RequestResponseCacheingApplication {

	public static void main(String[] args) {
		SpringApplication.run(RequestResponseCacheingApplication.class, args);
	}

}
