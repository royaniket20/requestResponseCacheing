package com.aniket.requestResponseCacheing.model;

import lombok.Data;

@Data
public class Student {
	private String id;
	private String name;
	private String subject;

}
